# System health event notifier

## Problem setting

Suppose you have a remote system behind an API.
The system is reactive: you send something to it, and it is expected to do something in result.
There is an API endpoint (*sink*) that receives requests and a callback (*source*) that sends responses.

Sink is just a python function that eventually calls an http endpoint.
Source could be implemented as webhook or a generator, but we'll focus on the latter case.

Let's say we have a notification channel that (*n-channel*) goes to humans, e.g. a telegram bot. 
Events are sent to n-channel using http api.

## Task

Write a script that periodically sends events to *sink* and expects response from *source*. 
If no response received, send something to *n-channel*. 

### Specifics

This is not required, if you really want to implement for something else, go ahead.

For source and sink use kafka-python https://pypi.org/project/kafka-python/. Source = kafkaProducer, sink = kafkaConsumer.

For n-channel use a telegram bot. Principle:
```
def send_notification(message):
  r = requests.get('https://telegram.org/...')
```

Script inputs:

1. config for kafka-python: server and topic 
2. config for telegram api: token and chat id where to send notifications.


## Criterions

1. Readability
2. Extensibility and ability to modify
3. Attention to edge cases. Everything that can possibly happen *will* happen.
4. Ability to decide things that are not clear on yourself. 
  Not all tasks are clear and sometimes you have to a) guess what people mean b) ask.