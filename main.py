import requests
import json
from time import sleep
from kafka import KafkaConsumer, KafkaProducer

KAFKA_SERVER = '127.0.0.1'
KAFKA_TOPIC = 'test_topic'
TELEGRAM_TOKEN = ''
TEL_CHATID = ''
KAFKA_PORT = '9092'
MSG = {'id': 123, 'msg': 'ping'}
sent_message_ids = []


def get_chat_id(tel_token: str) -> str:
    res = requests.get('https://api.telegram.org/bot' + tel_token + '/getUpdates')
    if res.status_code == 200:
        r = res.json()
        tel_chatid = r['result'][0]['message']['chat']['id']
        return str(tel_chatid)
    else:
        return 'error'


def send_notification(message: str, telegram_token: str = TELEGRAM_TOKEN, tel_chatid: str = TEL_CHATID) -> int:
    """Функция отправлят сообщение в телеграмм канал и возвращает код http ответа """
    if tel_chatid == '':
        tel_chatid = get_chat_id(telegram_token)
    else:
        if tel_chatid == 'error':
            raise Exception('Incorrect tegram settings')
    r = requests.get('https://api.telegram.org/bot' + telegram_token + '/sendMessage' + '?chat_id=' +
                     tel_chatid + '&text=' + message)
    # print(type(r.status_code))
    return r.status_code


def source(message: str, kafka_server: str = KAFKA_SERVER, kafka_topic: str = KAFKA_TOPIC,
           kafka_port: str = KAFKA_PORT):
    """'id': 54645654646, 'msg': 'ping'"""
    producer = KafkaProducer(bootstrap_servers=kafka_server + ':' + kafka_port)
    producer.send(kafka_topic, bytes(message, 'UTF-8'))


def sink():
    msg = consumer.poll(timeout_ms=1.0)
    return msg


consumer = KafkaConsumer(bootstrap_servers=KAFKA_SERVER + ':' + KAFKA_PORT)
consumer.subscribe([KAFKA_TOPIC])

while True:
    source(json.dumps(MSG))
    sent_message_ids.append(MSG['id'])
    sleep(3)
    answer = sink()
    if answer is not None:
        for value in answer:
            ans = json.loads(value)
            if ans['id'] in sent_message_ids:
                sent_message_ids.remove(ans['id'])
    if len(sent_message_ids) != 0:
        try:
            send_notification('Не вернулись: ' + ','.join(sent_message_ids))
        except Exception as error:
            print(error)
    sleep(3)
